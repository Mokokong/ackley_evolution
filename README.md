# Project Name: *Ackley Evolution*

## Implementation:
	* C++
	
## Premise
	* Implement a population that will continually evolve towards creating a single fit individual as evaluted by a fitness funtion

## Development environment:
	* Visual Studio 2017
	
## Completion:

### Deliverable 1: 100%

	* Create individuals who can evaluate their own fitness
	* View gene code of individuals
	* View fitness of each individual
	* Generate a population - user input population size. 

###Deliverable 2:
	*
		
## Optional completion:
	
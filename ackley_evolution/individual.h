/*
*		Author : Tiiso Mokokong 
*		Individual class
*		Features:

*			+ Phase 1 requirements:
*			- Each individual have a preset genome length only adjustable in the code
*			- Genome values range from 32.768 to - 32.768
*			- Each individual can evaluate its fitness using an Ackley function
*			- Each individual has its own unique id on the instance run
*
*			+Phase 2 requirements 
*
*
*
*
*
*/

// includes
#include "pch.h"


// # defines 
#define pi 3.141592654
#define eps 0.46400



// constants 
const int n = 10;// fixed genotype length.Genotype length can only be changed here 


using namespace std;



class individual
{
	private:
		// individual genome code
		double *_genCode;

		// value of fitness based on Ackley function
		double _howFit;

		// keeps track of number of individuals created
		static unsigned long int _census;

		// needed to  uniquely identify each individual
		unsigned long int _ID;

	public:
		// constructors 
		// basic constructor called primarily for child individuals
		individual();

		// used when generating initial population individuals
		individual(int k);
		~individual();

		// id set and get

		void setID();

		unsigned long int getID();

		// genome code get and set

		void set_genCode();

		void get_genCode();

		// get and set fitness

		void Ackley();

		double get_fitness() { return _howFit; }

};


unsigned long int individual::_census = 0;

individual::individual()
{
}

individual::individual(int k):
	_howFit( -99.99)
{
	// reserve genome code space 
	_genCode = new double[k];

	// update census
	_census++;

	// set id
	setID();
	// generate genome code data

	set_genCode();

	// evaluate fitness
	
	Ackley();

}
individual::~individual()
{
}

// ID setters and getters 

inline unsigned long int individual::getID()
{
	return _ID;
}

inline void individual::setID()
{
	_ID = _census;
}


// set and get genome code

void individual::set_genCode()
{

	for (int i = 0; i < n; i++)
	{
		//randomly generates numbers between -32.768 to 32.768
		//_genCode.push_back((((double(rand() % 4356)) / 66.00) - eps - 32.768));

		_genCode[i] = (((double(rand() % 4356)) / 66.00) - eps - 32.768);

	}
}

void individual::get_genCode()
{

	cout << "[" << endl;
	for (int i = 0; i < n; i++)
	{
		cout << "...."<< _genCode[i]<< "...."<<endl;
	}
	cout << "]" << endl;
}

//evaluates each individual's fitness 
void individual::Ackley()
{
	//will hold values of summations
	double sum1, sum2;

	//initialisation
	sum1 = sum2 = 0.0;

	// Perform Ackley function summations
	for (int j = 0; j < n; j++)
	{
		sum1 += pow((_genCode[j]), 2);
		sum2 += cos(2 * pi*(_genCode[j]));

	}

	// calculate exponent factors
	double expo_fac1 = -0.2 * sqrt((1.00 / n)*sum1);
	double expo_fac2 = (1.00 / n) * sum2;
	// debug
	/*cout<< "expof1: " << expo_fac1<< endl;
	cout<< "expof2: " << expo_fac2<< endl;*/

	double ack_exp1 = exp(expo_fac1);
	double ack_exp2 = exp(expo_fac2);
	// debug
	/*cout<< "ack_exp1: " << ack_exp1<< endl;
	cout<< "ack_exp2: " << ack_exp2<< endl;
	*/


	double ackley_val = (-20.0 * ack_exp1) - ack_exp2 + 20.0 + exp(1.00);
	_howFit = ackley_val;
	// debug
	/*cout << setprecision (9)<< ackley_val << "\nFUBAR\n";*/



}



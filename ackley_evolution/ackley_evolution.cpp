// ackley_evolution.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include "pch.h"
#include "individual.h"



int pop_size;
// pop_vector short for population vector
// holds individuals
vector<individual> pop_vector;


int main()
{

	int seedv = time(NULL);
	srand(seedv);
	// get vector size and create vector
	cout << "How big do you want your population to be: ";
	cin >> pop_size;
	pop_vector.reserve(pop_size);

	//create population
	for (int z = 0; z < pop_size; z++)
	{
		pop_vector.push_back(individual(n));

	}

	int res1;// response variable to seeing individual array contents
	cout << "\nEnter 0 to see each individuals unique genotype or 1 to skip this option :";
	cin >> res1;

	if (res1 == 0)
	{
		for (int index = 0; index < pop_size; index++)
		{
			cout << "\nAccessing individual :" << index << " for genotype..." << endl;
			(pop_vector.at(index)).get_genCode();

		}
	}

	cout << endl;

	int res2;// response variable to seeing fitness scores
	cout << "\nEnter 0 to see each individuals fitness score or 1 to skip this option :";
	cin >> res2;

	if (int(res2) == 0)
	{
		for (int index = 0; index < pop_size; index++)
		{
			cout << "\n=============================";
			cout << "\nIndividual: " << index << "\nID :" << (pop_vector.at(index)).getID();
			cout << "\nFitness score: " << setprecision(9) << (pop_vector.at(index)).get_fitness() << endl;
			cout << "=============================\n";
		}
	}


	std::cout << "Whiskey Tango Foxtrott!\n"; 

	_getch();


	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
